# ISO3166-service

## Summary

Simple microwebservice running on [hook.io](http://hook.io) which returns country names based on Country Code (ISO 3166 ALPHA2 e.g. ie, gb) and Language Code (ISO 639-1 codes e.g. en, ko, ja).

## Usage

```
<language>/<country code>"
Example; en/ie will return "Ireland"

<language>/all will return all countries with codes in that language.
Example; ru/all
```

Supported languages are English, Chinese, Korean, Japanese, Russian, & Indonesian. 

## Installation

* Setup hook.io account and create a new service.
* Set route as /:language/:iso
* Set source code language as Python (2.7).
* Set the code source as a Github Repo, Github Gist or copy the script to the Code Editor.

## Data

Note: Currently data is stored as a JSONArray String hardcoded. This is because hook.io currently does not have support for Python to use their datastore. Support for python in general is beta at this time.
***This needs to be changed***

The data is store as a JSONArray of Countries with country code (ISO 3166 ALPHA2) & country name by language,
English, Chinese, Korean, Japanese, Russian, & Indonesian.

```
Countries saved in the format;
"language code (ISO 639-1)": {
 	"<country code (ISO 3166 ALPHA2)>": "<Name of Country in specified language>"
 }

 Example;
 "ru": {
      "BD": "Бангладеш",
      "BE": "Бельгия",
      "BF": "Буркина-Фасо",
      "BG": "Болгария",
      "BA": "Босния и Герцеговина",
      "BB": "Барбадос",
      "WF": "Уоллес и Футана",
      .....
```
